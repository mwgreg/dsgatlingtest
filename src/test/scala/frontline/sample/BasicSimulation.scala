/*
 * Copyright 2011-2018 GatlingCorp (https://gatling.io)
 *
 * All rights reserved.
 */

package frontline.sample

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation extends Simulation {
  val httpConf = http
    .baseUrl("https://gamesvc.dev.destinationsolitaire.ext.mobilityware.com")

  val scn = scenario("scenario1")
    .exec(
      http("Page 0")
        .get("/get-server-info")
    )

  setUp(
    scn.inject(rampUsers(40000) during (30 seconds))
  ).protocols(httpConf)
}
